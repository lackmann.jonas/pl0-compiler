%{
#include<stdio.h>
#include "tree.hpp"
#include<string.h>
#include"y.tab.hpp"

/*
enum tokens{
        Comma,
        DOT,
        Semicolon,
        Assignop,
        Question,
        Exclam,
        LT,
        LEQ,
        GT,
        GEQ,
        Hashtag,
        Equals,
        Plus,
        Minus,
        Mul,
        Div,
        LPar,
        RPar,
        Digit,
        Ident,
        WHILE,
        DO,
        T_BEGIN,
        END,
        IF,
        THEN,
        CALL,
	VAR,
        PROC
        };
*/
%}

%%
"."	return T_DOT;
","	return T_Comma;
";"	return T_Semicolon;
":="	return T_Assignop;
"?"	return T_Question;
"!"	return T_Exclam;
"<"	return T_LT;
"<="	return T_LEQ;
">"	return T_GT;
">="	return T_GEQ;
"#"	return T_Hashtag;
"="	return T_Equals;
"+"	return T_Plus;
"-"	return T_Minus;
"*"	return T_Mul;
"/"	return T_Div;
"("	return T_LPar;
")"	return T_RPar;
"VAR" 	return T_VAR;
"PROCEDURE" return T_PROC;
"BEGIN" return T_BEGIN;
"END"	return T_END;
"IF"	return T_IF;
"THEN"	return T_THEN;
"WHILE"	return T_WHILE;
"ODD"   return T_ODD;
"DO"	return T_DO;
"CALL" 	return T_CALL;
"CONST" return T_CONST;

[0-9]+  yylval.ergebnis = atoi(yytext); return T_Digit;
[A-Za-z_][A-Za-z_0-9]* yylval.texschd=yytext; return T_Ident;
[\n\r\t];
%%

	
	int yywrap(){
		return 1;
	}
