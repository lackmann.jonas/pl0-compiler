#include <cstdlib>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
using namespace std;

enum types {expres, varD, blogg, stmt_end , stmt_assign , stmt_call , stmt_read , stmt_write , stmt_debug , stmt_jump_false , stmt_nop , stmt_jump };

struct s_ast {
    types type; // Art des Knotens
    string id; // Name des Bezeichners
    int stl; // Symtab−L e v e l
    int val; // Value für procs etc.
    struct s_ast * l;
    struct s_ast * r;
    struct s_ast * jump;
    string endL;
    string endR;
}; 

/*
// stmt: zweiger auf stmt knoten
struct s_ast_stmt {
    int type; // Art des Knotens
    string id; // Name des Bezeichners
    int stl; // Symtab−L e v e l
    int val; // Value für procs etc.
    struct s_ast_expr * expr;
    struct s_ast_stmt * next;
    struct s_ast_stmt * jump;
}; 

// expreschn: analog zu arithmetischen termen; zweiger auf expreschn knoten 
struct s_ast_expr {
    string id;
    struct s_ast_expr *l;
    struct s_ast_expr *r;
};

// var: zweiger auf var 
struct s_ast_var {
    int stl; // Symtab−Levels
    string str; // wird alles in den string gepackt 
    struct s_ast_var * next;
};

// blogg: zweiger auf var-knoten 
struct s_ast_blogg {
    int stl;
    struct s_ast_var *nextVar;
    struct s_ast_stmt *nextStmt;
    struct s_ast_blogg *nextBlogg;
};
*/
