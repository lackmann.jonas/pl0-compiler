#include <iostream>
#include "lex.yy.hpp"
#include "pl0-symtab.hpp"
#include <fstream>
#include <string>
#include "aschd.hpp"

int yylex();
int yyparse();
int yyerror(const char *s);
extern FILE*yyin;
using namespace std;

//typedef tree<string> myTree;
int main(int argc, char *argv[])
{
	FILE *fh;
	string filename = argv[1]; // ./pl-0 fLokalZuG.l
	//string filename ="fLokalZuG.l";
	fh = fopen(filename.c_str(), "r");
	yyin = fh;
	yyparse();

	/*tree<int> *t = new tree<int> (4711);
		t->ascii();
		t->postorder();*/
	return 0;
	
}

int yyerror(const char *s)
{
	cout << s;
	return 1;

}
