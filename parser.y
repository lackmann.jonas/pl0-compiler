%{
#define NULL 0
#include "codegen.hpp"
#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "pl0-symtab.hpp"
#include <vector>
int yylex(void);
void yyerror(const char*);
vector<s_ast> tree;
symtab st;
int varNr;
int procNr;
int stl;
int *ptr_stl = &stl;
int *ptr_varNr = &varNr;
int *ptr_procNr = &procNr;
%}
%union {
	char *texschd;
	int ergebnis;
	struct s_ast *ast;
};

%left T_Plus T_Minus
%left T_Mul T_Div
%token<texschd> T_Ident 
%token<var> T_CONST 
%type<texschd> constDec 
%type<ergebnis> varDec factor
%token<ergebnis> T_Digit T_LT T_LEQ T_GT T_GEQ T_Assign T_Mul T_Minus T_Plus T_Div T_Hashtag T_Equals T_Exclam T_Assignop T_Comma T_Semicolon T_LPar T_RPar T_Question T_VAR T_PROC T_CALL T_BEGIN T_IF T_THEN T_WHILE T_DO T_END T_ODD
//%type<statement>  statementLischd 
//%type<var> varN constN
//%type<blogg> blogg program procDec statement
//%token<blogg> T_DOT
//%type<expresschn> expresschn condischn term
%type<ast> statementLischd varN constN blogg program procDec statement expresschn condischn term
%token<ast> T_DOT

%%
program:	blogg T_DOT {s_ast program; program.r = $1; 
			for(int i=0; i < tree.size(); i++)
   				std::cout << tree.at(i).type << ' ';
			generateCode(tree);
			};

blogg:		{st.level_up();}
			constDec varDec procDec statement
			{st.level_down();};

constN:		| constN T_Comma T_Ident T_Equals T_Digit {st.insert($3, st_const,$5);}

constDec:	| T_CONST T_Ident T_Equals T_Digit constN T_Semicolon {st.insert($2, st_const, $4); s_ast var;
			 var.type = varD; var.stl = stl; var.val = $4; var.l = NULL; var.r = NULL; var.jump = $5;
			 tree.push_back(var);};
 
varN:		| varN T_Comma T_Ident {st.insert($3, st_var, varNr++);};

varDec:		| T_VAR T_Ident varN T_Semicolon {st.insert($2, st_var, varNr++);s_ast var;
			 var.type = varD; var.stl = stl; var.l = NULL; var.r = NULL; var.jump = NULL; tree.push_back(var);};

procDec:	| procDec T_PROC T_Ident T_Semicolon blogg T_Semicolon{st.insert($3, st_proc, ++procNr);}
			;	

statement:	| T_Ident T_Assignop expresschn {int sto; st.lookup($1, st_var, *ptr_stl, *ptr_varNr); 
			s_ast stmt; stmt.type = stmt_assign; stmt.stl = stl; stmt.id = $1; stmt.l = NULL; stmt.jump = NULL;
			stmt.r = $3; tree.push_back(stmt);}
		| T_CALL T_Ident {int sto; st.lookup($2, st_proc, *ptr_stl, *ptr_procNr); s_ast stmt; stmt.type = stmt_call; stmt.stl = stl;
		stmt.id = $2; stmt.l = NULL; stmt.r = NULL; stmt.jump = NULL; tree.push_back(stmt);}
		| T_Question T_Ident {s_ast stmt; stmt.stl = stl; stmt.id = $2; stmt.type = stmt_read; stmt.l = NULL;
		stmt.r = NULL; stmt.jump = NULL; tree.push_back(stmt);}
		| T_Exclam T_Ident {s_ast stmt; stmt.stl = stl; stmt.id = $2; stmt.type = stmt_read; 
		stmt.l = NULL; stmt.r = NULL; stmt.jump = NULL; tree.push_back(stmt);}
		| T_BEGIN statement statementLischd T_END
		| T_IF condischn T_THEN statement {s_ast stmt; stmt.stl = stl; s_ast expr; expr.l = $2; expr.r = NULL; expr.jump = NULL;
		stmt.r = $4; stmt.l = NULL; stmt.jump = NULL; tree.push_back(stmt);} // noch was mit stmt od expr tun? also expr in stmt speichern oder so
		| T_WHILE condischn T_DO statement {s_ast stmt; stmt.stl = stl; s_ast expr; expr.l = $2; stmt.r = $4; stmt.l = NULL;
		expr.r = NULL; stmt.jump = NULL; expr.jump = NULL; tree.push_back(stmt);} // noch was mit stmt od expr tun? also expr in stmt speichern oder so
		;

statementLischd:	| statementLischd T_Semicolon statement
					;
		
condischn: T_ODD expresschn
		| expresschn T_Equals expresschn {s_ast expr; expr.id = $2; expr.l = $1; expr.r = $3; tree.push_back(expr);}
		| expresschn T_Hashtag expresschn {s_ast expr; expr.id = $2; expr.l = $1; expr.r = $3; tree.push_back(expr);}
		| expresschn T_LT expresschn {s_ast expr; expr.id = $2; expr.l = $1; expr.r = $3; tree.push_back(expr);}
		| expresschn T_LEQ  expresschn {s_ast expr; expr.id = $2; expr.l = $1; expr.r = $3; tree.push_back(expr);}
		| expresschn T_GT expresschn {s_ast expr; expr.id = $2; expr.l = $1; expr.r = $3; tree.push_back(expr);}
		| expresschn T_GEQ  expresschn {s_ast expr; expr.id = $2; expr.l = $1; expr.r = $3; tree.push_back(expr);};

expresschn:	term {s_ast expr; expr.l = $1; tree.push_back(expr);}
		| expresschn T_Plus term {s_ast expr; expr.id = $2; expr.l = $3; expr.r = $1; tree.push_back(expr);}
		| expresschn T_Minus term {s_ast expr; expr.id = $2; expr.l = $3; expr.r = $1; tree.push_back(expr);};

term:		factor {s_ast expr; expr.endL = $1; tree.push_back(expr);}
		|term T_Mul factor {s_ast expr; expr.id = $2; expr.l = $1; expr.endR = to_string($3); expr.r = NULL; tree.push_back(expr);}
	    |term T_Div factor {s_ast expr; expr.id = $2; expr.l = $1; expr.endR = to_string($3); expr.r = NULL; tree.push_back(expr);}

factor:	T_Ident {st.lookup($1, st_var | st_const, *ptr_stl,*ptr_varNr);}
		| T_Digit 
		| T_LPar expresschn T_RPar;

%%
