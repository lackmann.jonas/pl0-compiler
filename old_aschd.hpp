#ifndef ASCHD_HPP
#define ASCHD_HPP
 
#include <cstdlib>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
using namespace std;

extern bool error;
 
enum Oper {
  Plus, Minus,
  Mul, Div,
  LT, GT, LEQ, GEQ, 
  Equals
};
 
class ASCHD;
  class Stmt;
    class NullStmt;
    class Blogg;
    class IfStmt;
    class WhileStmt;
    class NewStmt;
    class Assign;
    class Write;
  class Exp;
    class Id;
    class Num;
    class ArithOp;
    class CompOp;
    class Read;
    class Call;

class ASCHD {
  private:
    void addToDot(ostream& out, int& nextnode);
 
  protected:
    string nodeLabel;
    vector<ASCHD*> children;

    virtual void ASCHDchild(ASCHD* child) = 0;
 
  public:
    void writeDot(const char* fname);
 
    ASCHD() { nodeLabel = "EMPTY"; }
};
 
class Exp :public ASCHD {
  protected:
    void ASCHDchild(ASCHD* child) { children.push_back(child); }
 
  public:
    /* This is the method that must be overridden by all subclasses.
     * It should perform the computation specified by this node, and
     * return the resulting value that gets computed. */
    virtual Value eval() {
      if (!error) {
        error = true;
      }
      return Value();
    }
};
 
class Id :public Exp {
  private:
    string val;
 
  public:
    Id(const char* v) { 
      val = v;
      nodeLabel = "Exp:Id:" + val;
    }
 
    // Returns a reference to the stored string value.
    string& getVal() { return val; }
};
 
/* A literal number in the program. */
class Num :public Exp {
  private:
    int val;
 
  public:
    Num(int v) { 
      val = v;
      ostringstream label;
      label << "Exp:Num:" << val;
      nodeLabel = label.str();
    }
 
    // To evaluate, just return the number!
    Value eval() { return val; }
};
 
class ArithOp :public Exp {
  private:
    Oper op;
    Exp* left;
    Exp* right;
 
  public:
    ArithOp(Exp* l, Oper o, Exp* r);
 
    Value eval();
};
 
class CompOp :public Exp {
  private:
    Oper op;
    Exp* left;
    Exp* right;
 
  public:
    CompOp(Exp* l, Oper o, Exp* r);
};

class Read :public Exp {
  public:
    Read() { nodeLabel = "Exp:Read"; }
};
 
class Stmt :public ASCHD {
  private:
    Stmt* next;
 
  protected:
    void ASCHDchild(ASCHD* child) {
      children.insert(children.end()-1, child);
    }
 
  public:
    static Stmt* append(Stmt* a, Stmt* b);
 
    Stmt ();
 
    Stmt (Stmt* nextStmt) {
      if (nextStmt != NULL) children.push_back(nextStmt);
      next = nextStmt;
    }
 
    Stmt* getNext() { return next; }
    void setNext(Stmt* nextStmt) { 
      children.back() = nextStmt; 
      next = nextStmt;
    }
 
    bool hasNext() { return next != NULL; }
 
    /* This is the command that must be implemented everywhere to
     * execute this Stmt - that is, do whatever it is that this statement
     * says to do. */
    virtual void exec() {
      if (!error) {
        error = true;
      }
    }
};
 
/* This class is necessary to terminate a sequence of statements. */
class NullStmt :public Stmt {
  public:
    NullStmt() :Stmt(NULL) { 
      nodeLabel = "Stmt:Null";
    }

    void exec() { }
};
 
class Blogg :public Stmt {
  private:
    Stmt* body;
 
  public:
    Blogg(Stmt* b) { 
      nodeLabel = "Stmt:Blogg";
      body = b;
      ASCHDchild(body);
    }
};
 
class IfStmt :public Stmt {
  private:
    Exp* clause;
    Stmt* ifblogg;
 
  public:
    IfStmt(Exp* e, Stmt* ib) { 
      nodeLabel = "Stmt:If";
      clause = e;
      ifblogg = ib;
      ASCHDchild(clause);
      ASCHDchild(ifblogg);
    }
};

class WhileStmt :public Stmt {
  private:
    Exp* clause;
    Stmt* body;
   
  public:
    WhileStmt(Exp* c, Stmt* b) { 
      nodeLabel = "Stmt:While";
      clause = c;
      body = b;
      ASCHDchild(clause);
      ASCHDchild(body);
    }
};

/* An assignment statement. This represents a RE-binding in the symbol table. */
class Assign :public Stmt {
  private:
    Id* lhs;
    Exp* rhs;
   
  public:
    Assign(Id* l, Exp* r) { 
      nodeLabel = "Stmt:Assign";
      lhs = l;
      rhs = r;
      ASCHDchild(lhs);
      ASCHDchild(rhs);
    }
};

class Write :public Stmt {
  private:
    Exp* val;
 
  public:
    Write(Exp* v) { 
      nodeLabel = "Stmt:Write";
      val = v;
      ASCHDchild(val);
    }
 
    void exec() {
      Value res = val->eval();
      if (!error) {
      }
      getNext()->exec();
    }
};
 
class Call :public Exp {
  private:
    Exp* funexp;
    Exp* arg;
  
  public:
    Call(Exp* f, Exp* a) { 
      nodeLabel = "Exp:Call";
      funexp = f;
      arg = a;
      ASCHDchild(funexp);
      ASCHDchild(arg);
    }
};
 
#endif //ASCHD_HPP