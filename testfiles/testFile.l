CONST val1 =  7, val2 = 85;
VAR x, y, z;

PROCEDURE mul;
VAR mul1, mul2;

BEGIN
  mul1 := x;
  mul2 := y;
  z := 0;
  WHILE mul2 > 0 DO BEGIN
    IF ODD mul2 THEN z := z + mul1;
    mul1 := 2 * mul1;
    mul2 := mul2 / 2
  END;
  ! z
END;

PROCEDURE div;
VAR divisor, quotient, remainder;
BEGIN
  remainder := x; 
  quotient := 0;
  divisor := y;
  WHILE divisor <= remainder DO divisor := 2 * divisor; 
  WHILE divisor > y DO BEGIN  
    quotient := 2 * quotient;
    divisor := divisor / 2;
    IF divisor <= remainder THEN BEGIN
      remainder := remainder - divisor;
      quotient := quotient + 1
    END
  END;
  ! quotient;
  ! remainder
END;

BEGIN
  x := val1;
  y := val2;
  CALL mul;
  x := 25;
  y :=  3;
  CALL div
END.