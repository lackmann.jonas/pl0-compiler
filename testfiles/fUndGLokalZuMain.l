VAR a , d ;
PROCEDURE g ;
VAR c , d ;
BEGIN
a := 12;
c := 32;
d := 42;
DEBUG; (∗Hauptspeicher ∗)
! a ;
! c ;
! d ;
END;

PROCEDURE f ;
VAR b , d ;
BEGIN
a := 11;
b := 21;
d := 41;
DEBUG; (∗Hauptspeicher ∗)
CALL g ;
! a ;
! b ;
! d ;
END;

BEGIN
a := 10;
d := 40;
DEBUG; (∗Hauptspeicher ∗)
CALL f ;
! a ;
! d ;
END.