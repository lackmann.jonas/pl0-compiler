#include "aschd.hpp"
 
/* Adds this node and all children to the output stream in DOT format. 
 * nextnode is the index of the next node to add. */
void ASCHD::addToDot(ostream& out, int& nextnode) {
  int root = nextnode;
  ++nextnode;
  out << "\tn" << root << " [label=\"" << nodeLabel << "\"];" << endl;
  for (int i=0; i < children.size(); ++i) {
    int child = nextnode;
    children[i]->addToDot(out, nextnode);
    out << "\tn" << root << " -> n" << child << ";" << endl;
  }
}
 
/* Writes this ASCHD to a .dot file as named. */
void ASCHD::writeDot(const char* fname) {
  ofstream fout(fname);
  int nodes = 1;
  fout << "digraph ASCHD {" << endl;
  addToDot (fout, nodes);
  fout << "}" << endl;
  fout.close();
}
 
// ArithOp constructor
ArithOp::ArithOp(Exp* l, Oper o, Exp* r) { 
  op = o;
  left = l;
  right = r;
  nodeLabel = "Exp:ArithOp:";
  switch(o) {
    case Plus: nodeLabel += '+'; break;
    case Minus: nodeLabel += '-'; break;
    case Mul: nodeLabel += '*'; break;
    case Div: nodeLabel += '/'; break;
    default:  nodeLabel += "ERROR";
  }
  ASCHDchild(left);
  ASCHDchild(right);
}

Value ArithOp::eval() {
  int l = left->eval().num();
  int r = right->eval().num();
  switch(op) {
    case Plus: return l + r;
    case Minus: return l - r;
    case Mul: return l * r;
    case Div: 
      if (r != 0) return l / r;
      else if (!error) {
        error = true;
      }
      return Value();
    default:  return Value(); 
  }
}
 
CompOp::CompOp(Exp* l, Oper o, Exp* r) {
  op = o;
  left = l;
  right = r;
  nodeLabel = "Exp:CompOp:";
  switch(o) {
    case LT: nodeLabel += "<";  break;
    case GT: nodeLabel += ">";  break;
    case LEQ: nodeLabel += "<="; break;
    case GEQ: nodeLabel += ">="; break;
    case Equals: nodeLabel += "=";  break;
    default: nodeLabel += "ERROR"; break;
  }
  ASCHDchild(left);
  ASCHDchild(right);
}

Stmt* Stmt::append(Stmt* a, Stmt* b) {
  if (! a->hasNext()) return b;
  Stmt* last = a;
  while (last->getNext()->hasNext()) last = last->getNext();
  last->setNext(b);
  return a;
}
 
// Default constructor for Stmt.
// This HAS to be declared here because it uses NullStmt, which
// hasn't been defined yet in the header file!
Stmt::Stmt() {
  next = new NullStmt();
  children.push_back(next);
}