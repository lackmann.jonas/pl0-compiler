#include <iostream>
using namespace std;
#include "optree.h"

extern const int T_Plus, T_Minus, T_Mal, T_Div, T_LPar, T_RPar, T_Zahl;

opTreeEntry::opTreeEntry(int _t, int _v ): type(_t), value(_v) {}
opTreeEntry::opTreeEntry() {}

opTree::opTree(opTreeEntry e) {
	v = e;
}

opTree::opTree(opTreeEntry e, opTree *l) {
	v = e;
	append(l);
}

opTree::opTree(opTreeEntry e, opTree *l, opTree *r) {
	v = e;
	append(l), append(r);
}

int opTree::eval() {
	int value = 0;
	switch(v.type) {
		case T_Plus: value = ((opTree*)child[0])->eval() + ((opTree*)child[1])->eval(); break;
		case T_Minus: value = (child.size() == 1) ? - ((opTree*)child[0])->eval() : ((opTree*)child[0])->eval() - ((opTree*)child[1])->eval(); break;
		case T_Mal: value = ((opTree*)child[0])->eval() * ((opTree*)child[1])->eval(); break;
		case T_Div: value = ((opTree*)child[0])->eval() / ((opTree*)child[1])->eval(); break;
 		case T_Zahl: value = v.value; break;
	}
	return value;
}

void opTree::aassemble(ostream &o, int level) {
	switch(v.type) {
		case T_Plus:
			((opTree*)child[0])->aassemble(o, level + 1);
			((opTree*)child[1])->aassemble(o, level + 1);
			o << "\tadd\n";
			break;
		case T_Minus:
			((opTree*)child[0])->aassemble(o, level + 1);
			if (child.size() == 1) {
				o << "\tchs\n";
			}
			else {
				((opTree*)child[1])->aassemble(o, level + 1);
				o << "\tsub\n";
			}
			break;
		case T_Mal:
			((opTree*)child[0])->aassemble(o, level + 1);
			((opTree*)child[1])->aassemble(o, level + 1);
			o << "\tmult\n";
			break;
		case T_Div:
			((opTree*)child[0])->aassemble(o, level + 1);
			((opTree*)child[1])->aassemble(o, level + 1);
			o << "\tdiv\n";
			break;

		case T_Zahl:
			o << "\tloadc " << v.value << "\n";
			break;
	}
	if (!level)
		o << "\twrite" << endl;
}

ostream & operator << (ostream &o , const opTreeEntry &e) {
	switch(e.type) {
		case T_Plus: o << "+"; break;
		case T_Minus: o << "-"; break;
		case T_Mal: o << "*"; break;
		case T_Div: o << "/"; break;
		case T_Zahl: o << e.value; break;
		default: o << "error";
	}
	return o;
}


/*
int main() {
	opTree * t = new opTree(opTreeEntry(T_Minus));
//	t->append(new opTree(opTreeEntry(T_Zahl,2)));
	t->append(new opTree(opTreeEntry(T_Zahl,4)));
	t->postorder();
	cout << t->eval() << endl;
	t->aassemble();
}
*/
