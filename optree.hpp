#pragma once
#include <iostream>
using namespace std;
#include "tree.hpp"

class opTreeEntry {
public:
	int type;
	int value;
	opTreeEntry();
	opTreeEntry(int, int = 0);
	friend ostream & operator << (ostream &, const opTreeEntry &);
};

class opTree : public tree<opTreeEntry> {
public:
	opTree(opTreeEntry);
	opTree(opTreeEntry, opTree *);
	opTree(opTreeEntry, opTree *, opTree *);
	int eval();
	void aassemble(ostream & = cout, int level = 0);
};
