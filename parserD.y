%{
#include <stdio.h>
#include <stdlib.h>
#include "lex.yy.o"
#include "pl0-symtab.hpp"
void yyerror(char );
int yylex(void);

//Symboltabelle
symtab st;


%}
%token CONST VAR PROC CALL BEGIN IF THEN WHILE DO END
%token Ident Digit ODD LT LEQ GT GEQ Assign Mul Minus Plus Div Dot Hashtag Equals Exclam Assignop Comma Semicolon LPar RPar Question
%left Plus Minus
%left Mul Div
%%

program:        {aschd = $1;}
                program block '\n' {printf ("= %s\n", $2);}
                |;

block:           {st.level_up();}
                 |constDec varDec procDec statement 
                 {st.level_down();};

constDec:       |CONST Ident Equals Digit  Semicolon {st.insert($2, st_const, $4);};

constN:         |constN Comma Ident Equals Digit {st.insert($3, st_const, $5);}; 


varDec:       | VAR Ident Semicolon {st.insert($2, st_var, varNr++);};

varN:         | varN Comma Ident {st.insert($3, st_var, varNr++);}; 


procDec:        | PROC Ident Semicolon block Semicolon {st.insert($2, st_proc, ++global_proc_nr);};


statement:      
                | Ident Assignop expresschn {int stl, sto; st.lookup($1, st_var, &stl, &varnr);}
                | CALL Ident {int stl, sto; st.lookup($2, st_proc, &stl, &procnr);}
                | Question Ident 
                | Exclam Ident 
                | BEGIN statement statementLischd END 
                | IF condischn THEN statement  
                | WHILE condischn DO statement;
				
statmentLischd: | statmentLischd Semicolon statment
				| statment;

				
condischn:      ODD expresschn
                | expresschn 
                | expresschn Equals expresschn 
                | expresschn Hashtag expresschn 
                | expresschn LT expresschn 
                | expresschn LEQ  expresschn
                | expresschn GT expresschn 
                | expresschn GEQ  expresschn;

expresschn:     | expresschn Plus term 
                | expresschn Minus term 
                | term 
                | LPar expresschn RPar;

term:           |term Mul factor 
                |term Div factor 
                |factor ;

factor:         Ident {st.loopup($1, st_var | st_const, stl, varnr);}
                | Digit;


aschd:
%%

void yyerror(char *s)
{
        fprintf(stderr, "%s\n", s);
        return 0;
}

int main()
{
        return yyparse();
}
