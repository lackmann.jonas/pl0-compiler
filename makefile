pl-0:	y.tab.o lex.yy.o pl0-symtab.o pl-0.o
	clang++ y.tab.o lex.yy.o pl0-symtab.o pl-0.o -o pl-0

y.tab.o: parser.y 
	yacc parser.y -d -o y.tab.cpp
	clang++ -c y.tab.cpp -o y.tab.o

lex.yy.o: scanner.l
	lex -o lex.yy.cpp --header-file=lex.yy.hpp scanner.l
	clang++ -c lex.yy.cpp -o lex.yy.o

pl0-symtab.o: pl0-symtab.cpp 
	clang++ -c pl0-symtab.cpp

pl-0.o: pl-0.cpp
	clang++ -c pl-0.cpp

clean:
	rm lex.yy.* *.o y.tab.* *.o *.pl0 *.asm

